## [0.4.0] - 27-01-2022
### Added
* Added new `ai_models` app to host and manage AI models by the user;
* Added new APIs view: `requests_data`, `request_data_details`, `tasks_result` and `task_result_details`;
* Added Celery scheduler (beam) support;
* Added Docker deployment with GPU support and SSL
### Changed
* Refactored `MegaDetectorPredictor` and `ImagesTransformer`;
* Renamed and refactored animals detection API to `objects_detection`;
* venv updated (Python 3.6 to 3.8, TF 2.2 to 2.7 and other);


## [0.3.1] - 18-09-2020
### Changed
* Set fresh Gunicorn and Celery setup;


## [0.3.0] - 24-05-2020
### Changed
* Megadetector upgrade to v4.1.0;
* venv updated;


## [0.2.1] - 04-03-2020
### Changed
* Callback request to Trapper changed from GET to PUT;
* venv updated;


## [0.2.0] - 29-02-2020
### Added
* Added Sentry integration;
* Gunicorn, nginx and supervisor support;
* Extended API IO with Trapper AI model ID;
### Changed
* Improved urls.py to support static files;


## [0.1.1] - 24-02-2020
### Changed
* Improved `animal_detection` Celery task;


## [0.1.0] - 23-02-2020
### Added
* Initial support by REST API (basic auth) view for Animal detection with MegaDetector (V3);
