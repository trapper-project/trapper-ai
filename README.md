<p align="center">
  <img src="OSCF-TRAPPER-AI-logo.png">
</p>

<div align="center"> 
<font size="6"> Web-based application for managing jobs and AI models settings </font>
<br>
<hr> 
<img src="https://img.shields.io/badge/Python-3.11-blue"/>
<a href="https://demo.trapper-project.org/"><img src="https://img.shields.io/badge/Trapper-Demo-green" /></a>
<a href="https://trapper-project.readthedocs.io/en/latest/"><img src="https://img.shields.io/badge/Trapper-Documentation-yellow" /></a>
<a href="https://besjournals.onlinelibrary.wiley.com/doi/10.1111/2041-210X.12571"><img src="https://img.shields.io/badge/Trapper-Paper-blue.svg" /></a>
<a href="https://join.slack.com/t/trapperproject/shared_invite/zt-2f360a5pu-CzsIqJ6Y~iCa_dmGXVNB7A"><img src="https://img.shields.io/badge/Trapper-Slack-orange" /></a>
<a href="https://gitlab.com/trapper-project/trapper/-/blob/master/LICENSE"><img src="https://img.shields.io/badge/Licence-GPLv3-pink" /></a>
<br><br>
</div>

## Table of Contents

[Overview](https://gitlab.com/oscf/trapper-ai#-overview) |
[Installation](https://gitlab.com/oscf/trapper-ai#-installation) |
[Demo](https://gitlab.com/oscf/trapper-ai#-demo) |
[Documentation](https://gitlab.com/oscf/trapper-ai#-documentation) | 
[Who is using TRAPPER?](https://gitlab.com/oscf/trapper-ai#-who-is-using-trapper) | 
[Funders and Partners](https://gitlab.com/oscf/trapper-ai#-funders-and-partners) | 
[Support](https://gitlab.com/oscf/trapper-ai#-support) | 
[License](https://gitlab.com/oscf/trapper-ai#-license)

## 🐺 Overview

TrapperAI Manager is responsible for providing a web-based application for configuring AI models, communicating with **TrapperAI Worker(s)**, and exploring the statuses of detection and classification tasks.

An admin user can define a new AI model profile by uploading a weights file (e.g., *.pt file), setting the batch size, thresholds, and availability status manually.

TrapperAI Manager organizes the queue ecosystem using RabbitMQ + Redis, along with Celery workers and Flower for asynchronous task processing and monitoring. All results are stored in PostgreSQL. Communication between TrapperAI Manager and [TRAPPER (Trapper Expert)](https://gitlab.com/oscf/trapper-cs) is facilitated through a dedicated API.

## 📥 Installation

In order to deploy TrapperAI using docker you will need Docker and docker-compose installed on your system.

#### Configuration

You will need to create `.env` file containing configuration options for TrapperAI

- `DB_USER` required; database username
- `DB_PASS` required; database password
- `SECRET_KEY` required; secret key used internally by Django
- `BUILD_IMAGES` optional; when set to `1` all docker images will be build locally, otherwise images will be pulled from docker registry.
  By default, images are pulled from docker registry. Make sure that docker is authorized to pull images from `oscf/trapper-ai` registry,
  for example by logging in using `docker login registry.gitlab.com`
- `ACTIVE_BRANCH` optional; default: `master`; Branch to use for pulling images.
- `FLOWER_PASSWORD` password for flower
- `FLOWER_USERNAME`username for flower
- `APP_PORT` application port used when not using SSL
- `RABBIT_PORT` RabbitMQ port (default: 8072)
- `COMPOSE_PROJECT_NAME` prefix used eg. for docker-compose container names, defaults to `trapper-ai`
- `IMAGE_NAME` defaults to `registry.gitlab.com/oscf/trapper-ai`
- `TRAPPERAI_LOG_LEVEL` defaults to `info`
- `DEV_MODE` if set to `1` project directory will be mounted as a docker volume. Gunicorn and celery workers will be restarted on each code change.
- `ENABLE_SSL` if set to `1`, SSL is enabled; app is served on ports `80` and `443`
- `AUTO_SSL` if set to `1` certificates will be fetched and renewed automatically from Let's Encrypt
  If disabled, you are expected to provide `./certs/privkey.pem` and `./certs/fullchain.pem` certificate files
- `TRAPPERAI_DOMAIN` domain used for SSL; required if SSL is enabled
- `ALLOWED_HOSTS` list of allowed hosts for django separated by `|` or `*` indicating that all host are allowed (default)
- `MOUNT_VOLUMES` if set to `1` volumes will be mounted to local file system, otherwise mounted volumes will be used
- `VOLUMES_DIR` defaults to `./volumes` and is used as mount point for volumes when `MOUNT_VOLUMES` is enabled

#### TrapperAI deployment commands

You can use `./trapperai.sh` script to deploy entire TrapperAI stack

- `./trapperai.sh start` start entire stack using `.env` config
- `./trapperai.sh start-i` start "interactive", so stack will be stopped when you use ctrl+c
- `./trapperai.sh logs` attach to container logs for all containers
- `./trapperai.sh stop` stop running stack
- `./trapperai.sh shell` starts new interactive bash session in the `web` container

## 🌐 Demo
[Trapper Expert ](https://demo.trapper-project.org) demo instance does not include **Trapper Citizen Science** or **Trapper AI** yet.

## 📝 Documentation

[TRAPPER](https://trapper-project.readthedocs.io) documentation.

If you are using TRAPPER or TrapperAI, please cite our work:

> Bubnicki, J.W., Churski, M. and Kuijper, D.P.J. (2016), trapper: an open source web-based application to manage camera trapping projects. Methods Ecol Evol, 7: 1209-1216. https://doi.org/10.1111/2041-210X.12571

> Choiński, M., Rogowski, M., Tynecki, P., Kuijper, D.P.J., Churski, M., Bubnicki, J.W. (2021). A First Step Towards Automated Species Recognition from Camera Trap Images of Mammals Using AI in a European Temperate Forest. In: Saeed, K., Dvorský, J. (eds) Computer Information Systems and Industrial Management. CISIM 2021. Lecture Notes in Computer Science(), vol 12883. Springer, Cham. https://doi.org/10.1007/978-3-030-84340-3_24

For more news about TRAPPER please visit the [Open Science Conservation Fund (OSCF) website](https://os-conservation.org) and [OSCF LinkedIn profile](https://www.linkedin.com/company/os-conservation/).

## 🏢 Who is using TRAPPER?
* Mammal Research Institute Polish Academy of Sciences;
* Karkonosze National Park;
* Swedish University of Agricultural Sciences;
* Svenska Jägareförbundet;
* Meles Wildbiologie;
* University of Freiburg Wildlife Ecology and Management;
* Bavarian Forest National Park;
* Georg-August-Universität Göttingen;
* KORA - Carnivore Ecology and Wildlife Management;
* and many more individual scientists and ecologies;

## 💲 Funders and Partners
<p align="center">
  <img src="TRAPPER-funds.png">
</p>
<p align="center">
  <img src="TRAPPER-partners.png">
</p>

## 🤝 Support

Feel free to add a new issue with a respective title and description on the [TRAPPER issue tracker](https://gitlab.com/trapper-project/trapper/-/issues). If you already found a solution to your problem, we would be happy to review your pull request.

If you prefer direct contact, please let us know: `contact@os-conservation.org`

We also have [TRAPPER Mailing List](https://groups.google.com/d/forum/trapper-project) and [TRAPPER Slack](https://join.slack.com/t/trapperproject/shared_invite/zt-2f360a5pu-CzsIqJ6Y~iCa_dmGXVNB7A).

## 📜 License

Read more in [TRAPPER License](https://gitlab.com/oscf/trapper-ai/-/blob/master/LICENSE).
