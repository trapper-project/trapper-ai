#!/bin/bash

set -e

exec ./bin/run.sh celery -A trapper_ai worker -l "$TRAPPERAI_LOG_LEVEL" -Q downloads --concurrency=5

