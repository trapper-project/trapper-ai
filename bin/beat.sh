#!/bin/bash

set -e

exec ./bin/run.sh celery -A trapper_ai beat -l "$TRAPPERAI_LOG_LEVEL"

