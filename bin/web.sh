#!/bin/bash

set -e

echo "${0}: running migrations."
python manage.py makemigrations --merge --noinput
python manage.py migrate --noinput

echo "${0}: collecting statics."

python manage.py collectstatic --noinput

gunicorn_reload=()
if [ "$DEV_MODE" = 1 ]; then
  echo "Starting gunicorn in dev mode (auto reloading)"
  gunicorn_reload+=("--reload")
fi

WEB_CONCURRENCY=${WEB_CONCURRENCY:-8}

exec gunicorn trapper_ai.wsgi:application \
    --name trapper_ai \
    --bind "0.0.0.0:8000" \
    --timeout 600 \
    --workers "$WEB_CONCURRENCY" \
    --log-level="$TRAPPERAI_LOG_LEVEL" \
    --log-file - \
    --access-logfile - \
    --error-logfile - \
    --capture-output --enable-stdio-inheritance \
    "${gunicorn_reload[@]}"
