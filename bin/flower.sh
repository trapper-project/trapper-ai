#!/bin/bash

set -e

exec ./bin/run.sh celery -A trapper_ai flower \
  --port=5566 \
  --url_prefix=flower \
  --basic_auth="$FLOWER_USERNAME:$FLOWER_PASSWORD" \
  --broker-api="$REDIS_LOCATION" \
  -l info
