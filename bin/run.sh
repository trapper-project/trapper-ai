#!/bin/bash

set -e

export REMAP_SIGTERM=SIGQUIT

if [ "$DEV_MODE" = 1 ]; then
  exec watchmedo auto-restart --recursive -d . -p '*.py' -- python -m "$@"
else
  exec python -m "$@"
fi