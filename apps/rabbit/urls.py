from django.urls import path

from .views import RabbitUserView, RabbitTopicView, RabbitResourceView, RabbitVhostView


urlpatterns = [
    path("user/", RabbitUserView.as_view(), name="rabbit_user"),
    path("topic/", RabbitTopicView.as_view(), name="rabbit_topic"),
    path("resource/", RabbitResourceView.as_view(), name="rabbit_resource"),
    path("vhost/", RabbitVhostView.as_view(), name="rabbit_vhost"),
]
