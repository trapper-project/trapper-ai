from rest_framework import serializers


class RabbitAuthRequestSerializer(serializers.Serializer):
    username = serializers.CharField()
    password = serializers.CharField()

    def create(self, validated_data):
        return validated_data


class RabbitResourceRequestSerializer(serializers.Serializer):
    username = serializers.CharField()
    vhost = serializers.CharField()
    resource = serializers.CharField()
    name = serializers.CharField()
    permission = serializers.CharField()
    tags = serializers.CharField()

    def create(self, validated_data):
        return validated_data
