from django.shortcuts import HttpResponse
from rest_framework.response import Response

from rest_framework.authentication import SessionAuthentication, BasicAuthentication
from rest_framework.permissions import IsAuthenticated
from rest_framework.views import APIView, Http404
from rest_framework import status
from rest_framework.request import Request

from django.contrib.auth import authenticate


# Create your views here.
class RabbitUserView(APIView):
    authentication_classes = []
    permission_classes = []

    def post(self, request: Request) -> Response:
        username = request.data["username"]
        password = request.data["password"]
        user = authenticate(username=username, password=password)

        if user:
            return HttpResponse("allow")

        return HttpResponse("deny")


class RabbitVhostView(APIView):
    authentication_classes = []
    permission_classes = []

    def post(self, request: Request) -> Response:
        return HttpResponse("allow")


class RabbitResourceView(APIView):
    authentication_classes = []
    permission_classes = []

    def post(self, request: Request) -> Response:
        return HttpResponse("allow")


class RabbitTopicView(APIView):
    authentication_classes = []
    permission_classes = []

    def post(self, request: Request) -> Response:
        return HttpResponse("allow")
