from typing import Optional
from uuid import UUID

import structlog
from celery import shared_task
from django.core.files.base import ContentFile
from django.db.models import Q
from django.utils import timezone
from requests import RequestException

from apps.ai_models.models import AIRuntime
from trapper_ai.celery import app

from .models import PredictionJob, Resource, ResourceBatch
from .utils import ImageDownloader

from django.db import transaction

from django.utils.translation import gettext_lazy as _

logger = structlog.get_logger(__name__)


class BatchPredictionScheduler:
    error_messages = {
        "job_not_defined": _("Job not defined for batch"),
        "model_not_defined": _("AI model not defined for batch"),
        "runtime_not_defined": _("Runtime not defined for batch"),
    }

    def schedule_batch_prediction(self, batch_id: UUID):
        batch = ResourceBatch.objects.get(id=batch_id)

        if not batch.job:
            logger.error(self.error_messages["job_not_defined"], batch=batch_id)
            batch.set_status(
                ResourceBatch.ProcesingStatus.FAILED,
                self.error_messages["job_not_defined"],
            )
            return

        if not batch.job.ai_model_id:
            logger.error(self.error_messages["model_not_defined"], batch=batch_id)
            batch.set_status(
                ResourceBatch.ProcesingStatus.FAILED,
                self.error_messages["model_not_defined"],
            )
            return

        runtime: Optional[AIRuntime] = batch.job.ai_model_id.runtime

        if not runtime:
            logger.error(self.error_messages["runtime_not_defined"], batch=batch_id)
            batch.set_status(
                ResourceBatch.ProcesingStatus.FAILED,
                self.error_messages["runtime_not_defined"],
            )
            return

        q = runtime.get_runtime_queue_name()

        app.send_task(
            name="trapperai_runtime.run_batch_prediction",
            kwargs={"batch_id": batch_id},
            queue=q,
        )


@shared_task(name="refresh_prediction_job_status")
def refresh_prediction_job_status(job_id: UUID):
    job = PredictionJob.objects.get(id=job_id)

    log = logger.bind(job_id=job.id)

    resources = Resource.objects.filter(Q(job=job) | Q(resource_batch__job=job))

    failed_resources = (
        resources.filter(status=Resource.ResourceProcessingStatus.FAILED)
        .values_list("pk")
        .order_by()
        .distinct()
        .count()
    )
    if failed_resources > job.processing_error_limit:
        log.warn("Failed resources limit exceeded, failing job")
        job.update_status(PredictionJob.PredictionJobStatus.FAILED)

        job.batches.all().filter(
            processing_status__in=[
                ResourceBatch.ProcesingStatus.CREATED,
                ResourceBatch.ProcesingStatus.IN_PROGRESS,
            ]
        ).update(processing_status=ResourceBatch.ProcesingStatus.CANCELED)

        return

    with transaction.atomic():
        finished_count = (
            job.batches.all()
            .filter(
                processing_status__in=[
                    ResourceBatch.ProcesingStatus.SUCCEEDED,
                ]
            )
            .count()
        )
        failed_count = (
            job.batches.all()
            .filter(
                processing_status__in=[
                    ResourceBatch.ProcesingStatus.FAILED,
                ]
            )
            .count()
        )
        all_count = job.batches.all().count()

        if failed_count == all_count:
            log.warn("All resources failed, failing job")
            job.update_status(PredictionJob.PredictionJobStatus.FAILED)
        elif finished_count + failed_count == all_count:
            log.info("All resources processed")
            job.update_status(PredictionJob.PredictionJobStatus.DONE)
            return

    with transaction.atomic():
        if (
            job.batches.all()
            .filter(processing_status__in=[ResourceBatch.ProcesingStatus.IN_PROGRESS])
            .exists()
        ):
            log.info("Job still in progress")
            job.update_status(PredictionJob.PredictionJobStatus.IN_PROGRESS)
            return


def download_resource_sync(resource_id: str):
    resource = Resource.objects.get(pk=resource_id)

    log = logger.bind(resource_id=resource_id)

    resource.stage = Resource.ResourceProcessingStage.IMAGE_CRAWLING
    resource.save(update_fields=["stage"])
    resource.update_status(Resource.ResourceProcessingStatus.IN_PROGRESS)
    for attempt in range(1, 5):
        log.info("Downloading resource", attempt=attempt + 1)
        try:
            img_data, image_name = ImageDownloader(
                sample_url=resource.sample_url,
            ).get_image_contents()

            logger.info(f"After ImageCrawler")

            with ContentFile(img_data) as file_content:
                resource.origin_image.save(image_name, file_content)
                resource.stage = Resource.ResourceProcessingStage.MODEL_PREDICTION
                resource.save(update_fields=["origin_image", "modified_date", "stage"])

            logger.info(f"After image saving")
            return
        except RequestException as ex:
            log.error("Resource download failed", ex=ex)
    resource.update_status(
        Resource.ResourceProcessingStatus.FAILED, "Resource download failed"
    )


def resource_batch_download_finished(batch_id: str):
    batch = ResourceBatch.objects.get(id=batch_id)

    logger.info("Finalizing batch download", batch_id=batch_id)

    batch.processing_stage = ResourceBatch.ProcessingStage.RESOURCE_DOWNLOADING_FINISHED
    batch.download_end_timestamp = timezone.now()
    batch.save()
    BatchPredictionScheduler().schedule_batch_prediction(batch.pk)
    refresh_prediction_job_status.delay(batch.job_id)


@app.task(
    bind=True,
    max_retries=5,
    default_retry_delay=60,
    retry_on=[Resource.DoesNotExist, ResourceBatch.DoesNotExist],
    queue="downloads",
)
def download_images(self, batch_id: str) -> None:
    batch = ResourceBatch.objects.get(id=batch_id)

    if batch.job.ai_model_id.enable_image_pass_through:
        logger.info(f"Skipping download for batch {batch.id}")
        batch.set_status(ResourceBatch.ProcesingStatus.IN_PROGRESS)
        batch.set_stage(ResourceBatch.ProcessingStage.RESOURCE_DOWNLOADING_FINISHED)
        batch.resources.all().update(
            stage=Resource.ResourceProcessingStage.MODEL_PREDICTION,
            status=Resource.ResourceProcessingStatus.IN_PROGRESS,
        )
        batch.download_start_timestamp = timezone.now()
        batch.download_end_timestamp = timezone.now()
        batch.save()
        BatchPredictionScheduler().schedule_batch_prediction(batch.pk)
        refresh_prediction_job_status.delay(batch.job_id)
        return

    logger.info(f"Downloading all resources in batch {batch.id}")

    try:
        batch.set_status(ResourceBatch.ProcesingStatus.IN_PROGRESS)
        batch.set_stage(ResourceBatch.ProcessingStage.RESOURCE_DOWNLOADING)
        batch.download_start_timestamp = timezone.now()
        batch.download_end_timestamp = None
        batch.save()

        for resource_id in batch.resources.all().values_list("id", flat=True):
            download_resource_sync(resource_id)

        resource_batch_download_finished(batch_id)

    except Exception as e:
        batch.set_status(ResourceBatch.FAILED)

        raise self.retry(exc=e, countdown=5)
