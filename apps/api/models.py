import uuid

from django.db import models

from apps.ai_models.models import PredictionModel
from django.utils.translation import gettext_lazy as _

from django.core.validators import MinValueValidator

from django.utils import timezone


class PredictionJob(models.Model):
    class PredictionJobStatus(models.IntegerChoices):
        INITIAL = 0, _("Initial")
        IN_PROGRESS = 1, _("In progress")
        DONE = 2, _("Done")
        FAILED = 3, _("Failed")
        CANCELED = 900, _("Canceled")

    class PredictionJobStage(models.IntegerChoices):
        INITIAL = 0, _("Initial")
        IN_PROGRESS = 100, _("In progress")
        FINISHED = 900, _("Finished")

    id = models.UUIDField(
        primary_key=True,
        default=uuid.uuid4,
        editable=False,
        help_text=_("Unique auto-generated identifier of the prediction job"),
    )
    ai_model_id = models.ForeignKey(
        PredictionModel,
        verbose_name=_("Prediction model"),
        related_name="jobs",
        on_delete=models.SET_NULL,
        null=True,
    )

    processing_error_msg = models.TextField(
        _("Processing error message"),
        blank=True,
        default="",
        help_text=_(
            "Additional informations about job failure or cancelation, if available"
        ),
    )
    status = models.SmallIntegerField(
        _("Processing status"),
        default=PredictionJobStatus.INITIAL,
        choices=PredictionJobStatus.choices,
    )

    stage = models.SmallIntegerField(
        _("Processing stage"),
        default=PredictionJobStage.INITIAL,
        choices=PredictionJobStage.choices,
    )

    batch_size = models.IntegerField(
        default=1,
        validators=[MinValueValidator(1)],
        help_text=_("Maximum number of resources processed at the same time"),
    )
    processing_error_limit = models.IntegerField(
        default=0,
        help_text=_(
            "Maximum number of resource processing errors after which job will be canceled"
        ),
    )

    created_date = models.DateTimeField(_("Creation date"), auto_now_add=True)
    modified_date = models.DateTimeField(_("Modification date"), auto_now=True)

    start_time = models.DateTimeField(_("Processing start time"), null=True, blank=True)
    end_time = models.DateTimeField(_("Processing end time"), null=True, blank=True)

    def update_status(
        self, new_status: PredictionJobStatus, processing_error_msg: str = ""
    ) -> None:
        self.processing_error_msg = processing_error_msg
        if new_status != self.status:
            if new_status == self.PredictionJobStatus.IN_PROGRESS:
                self.update_stage(PredictionJob.PredictionJobStage.IN_PROGRESS)
            elif new_status == self.PredictionJobStatus.DONE:
                self.update_stage(PredictionJob.PredictionJobStage.FINISHED)

        self.status = new_status
        self.save(update_fields=["status", "processing_error_msg"])

    def update_stage(self, stage: PredictionJobStage) -> None:
        if self.stage != stage:
            if stage == self.PredictionJobStage.IN_PROGRESS:
                self.start_time = timezone.now()
            elif stage == self.PredictionJobStage.FINISHED:
                self.end_time = timezone.now()
        self.stage = stage

        self.save(update_fields=["stage", "start_time", "end_time"])

    @property
    def processing_time(self):
        if not self.start_time:
            return None
        if not self.end_time:
            return None

        return self.end_time - self.start_time

    @property
    def batches_finished(self):
        return self.batches.filter(
            processing_stage=ResourceBatch.ProcessingStage.FINISHED
        ).count()

    @property
    def batches_total(self):
        return self.batches.count()

    @property
    def batches_progress(self):
        batches_total = self.batches_total
        batches_finished = self.batches_finished

        if not batches_total:
            return None

        return 100 * batches_finished / batches_total

    def __str__(self):
        return f"{self.created_date} {str(self.id)}"

    class Meta:
        verbose_name = "Prediction job"
        verbose_name_plural = "Prediction jobs"

        ordering = ["-created_date"]


class ResourceBatch(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    job = models.ForeignKey(
        PredictionJob, on_delete=models.CASCADE, null=True, related_name="batches"
    )

    class ProcessingStage(models.IntegerChoices):
        CREATED = 100, _("Created")
        RESOURCE_DOWNLOADING = 200, _("Downloading resources")
        RESOURCE_DOWNLOADING_FINISHED = 250, _("Resource downloading finished")
        PREDICTION = 300, _("Prediction")
        FINISHED = 400, _("Finished")

    class ProcesingStatus(models.IntegerChoices):
        CREATED = 100, _("Created")
        IN_PROGRESS = 200, _("In progress")
        SUCCEEDED = 300, _("Succeeded")
        FAILED = 400, _("Failed")
        CANCELED = 500, _("Canceled")

    processing_stage = models.SmallIntegerField(
        _("Processing stage"),
        choices=ProcessingStage.choices,
        default=ProcessingStage.CREATED,
    )
    processing_status = models.SmallIntegerField(
        _("Processing status"),
        choices=ProcesingStatus.choices,
        default=ProcesingStatus.CREATED,
    )

    processing_error_msg = models.TextField(
        _("Processing error message"),
        blank=True,
        default="",
        help_text=_(
            "Additional informations about batch failure or cancelation, if available"
        ),
    )

    created_date = models.DateTimeField(_("Creation date"), auto_now_add=True)
    modified_date = models.DateTimeField(_("Modification date"), auto_now=True)

    download_start_timestamp = models.DateTimeField(
        _("Download start timestamp"), blank=True, null=True
    )
    download_end_timestamp = models.DateTimeField(
        _("Download end timestamp"), blank=True, null=True
    )
    prediction_start_timestamp = models.DateTimeField(
        _("Prediction start timestamp"), blank=True, null=True
    )
    prediction_end_timestamp = models.DateTimeField(
        _("Prediction end timestamp"), blank=True, null=True
    )

    class Meta:
        verbose_name = _("Resource batch")
        verbose_name_plural = _("Resource batches")

        ordering = ["-created_date"]

    def set_stage(self, stage: ProcessingStage):
        self.processing_stage = stage
        self.save(update_fields=["processing_stage", "modified_date"])

    def set_status(self, status: ProcesingStatus, msg=""):
        self.processing_status = status
        self.processing_error_msg = msg
        self.save(
            update_fields=["processing_status", "modified_date", "processing_error_msg"]
        )

    def get_processing_stage_code(stage_label: str):
        return {v: k for v, k in ResourceBatch.ProcessingStage.choices.items()}.get(
            stage_label
        )

    @property
    def downloading_time(self):
        if not self.download_start_timestamp:
            return None
        if not self.download_end_timestamp:
            return None

        return self.download_end_timestamp - self.download_start_timestamp

    @property
    def prediction_time(self):
        if not self.prediction_start_timestamp:
            return None
        if not self.prediction_end_timestamp:
            return None

        return self.prediction_end_timestamp - self.prediction_start_timestamp


class Resource(models.Model):
    class ResourceProcessingStage(models.IntegerChoices):
        INITIAL = 0, _("Initial")
        IMAGE_CRAWLING = 100, _("Image downloading")
        IMAGE_PREPROCESSING = 200, _("Image preprocessing")
        MODEL_PREDICTION = 300, _("Model prediction")
        FINISHED = 400, _("Finished")

    class ResourceProcessingStatus(models.IntegerChoices):
        INITIAL = 0, _("Initial")
        IN_PROGRESS = 100, _("In progress")
        FINISHED = 200, _("Finished")
        FAILED = 300, _("Failed")

    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)

    job = models.ForeignKey(
        PredictionJob,
        verbose_name=_("Prediction job"),
        related_name="tasks",
        on_delete=models.CASCADE,
        null=True,
    )

    sample_id = models.IntegerField(
        _("Sample ID"), help_text=_("Sample ID in external system")
    )
    sample_url = models.URLField(
        _("Sample URL"),
        help_text=_(
            "Resource URL in external system. Must be ccessible from TrapperAI instance"
        ),
    )

    origin_image = models.FileField(
        _("Trapper image"),
        upload_to="transformed_images",
        null=True,
        blank=True,
        help_text=_("Image downloaded from external system"),
    )

    prediction_results = models.TextField(_("Prediction results"), default="")
    processing_error_msg = models.TextField(
        "Processing error message",
        blank=True,
        default="",
        help_text=_(
            "Additional informations about resource processing failure or cancelation, if available"
        ),
    )
    status = models.SmallIntegerField(
        _("Processing status"),
        default=ResourceProcessingStatus.INITIAL,
        choices=ResourceProcessingStatus.choices,
    )
    stage = models.SmallIntegerField(
        _("Processing stage"),
        default=ResourceProcessingStage.INITIAL,
        choices=ResourceProcessingStage.choices,
    )

    resource_batch = models.ForeignKey(
        ResourceBatch,
        null=True,
        blank=True,
        on_delete=models.CASCADE,
        related_name="resources",
    )

    created_date = models.DateTimeField(_("Creation date"), auto_now_add=True)
    modified_date = models.DateTimeField(_("Modification date"), auto_now=True)

    def update_status(self, new_status: int, processing_error_msg: str = "") -> None:
        """
        Update status method
        """

        self.processing_error_msg = processing_error_msg
        self.status = new_status
        self.save(update_fields=["status", "processing_error_msg"])

    def __str__(self):
        return str(self.id)

    class Meta:
        verbose_name = _("Resource")
        verbose_name_plural = _("Resources")
        ordering = ["-created_date"]
