import base64
import json
import cv2
from django.contrib import admin
import numpy as np
import requests

from apps.api.tasks import download_images

from .models import PredictionJob, Resource, ResourceBatch

from django.utils.html import format_html


@admin.register(PredictionJob)
class PredictionJobAdmin(admin.ModelAdmin):
    list_display = [
        "id",
        "ai_model_id",
        "status",
        "stage",
        "created_date",
        "batches_progress",
        "processing_time",
    ]

    list_filter = [
        "status",
        "stage",
    ]

    search_fields = ["ai_model_id"]

    fieldsets = (
        (
            "Basic data",
            {
                "fields": ("ai_model_id",),
            },
        ),
        (
            "Statuses",
            {
                "fields": (
                    "processing_error_msg",
                    "status",
                    "stage",
                    "processing_error_limit",
                ),
            },
        ),
        (
            "Dates",
            {
                "fields": ("created_date", "modified_date"),
            },
        ),
    )

    readonly_fields = [
        "created_date",
        "modified_date",
        "processing_error_msg",
        "processing_error_limit",
    ]


@admin.register(Resource)
class ResourceAdmin(admin.ModelAdmin):
    list_display = [
        "id",
        "job",
        "sample_id",
        "sample_url",
        "status",
        "stage",
        "created_date",
    ]

    list_filter = [
        "status",
        "stage",
    ]

    search_fields = ["id", "sample_id"]

    fieldsets = (
        (
            "Basic data",
            {
                "fields": ("id", "job", "sample_id", "sample_url", "origin_image"),
            },
        ),
        ("Processing data", {"fields": ("resource_batch",)}),
        ("AI data", {"fields": ("image", "prediction_results")}),
        (
            "Statuses",
            {
                "fields": (
                    "processing_error_msg",
                    "status",
                    "stage",
                ),
            },
        ),
        (
            "Dates",
            {
                "fields": ("created_date", "modified_date"),
            },
        ),
    )

    readonly_fields = ["id", "created_date", "modified_date", "image", "origin_image"]

    def image(self, obj: Resource):
        if not obj.origin_image and not obj.job.ai_model_id.enable_image_pass_through:
            return None

        if obj.job.ai_model_id.enable_image_pass_through:
            with requests.get(obj.sample_url, timeout=25) as r:
                img = np.asarray(bytearray(r.content), dtype="uint8")
                img = cv2.imdecode(img, cv2.IMREAD_COLOR)
        else:
            img = cv2.imread(obj.origin_image.path)

        aspect_ratio = img.shape[1] / img.shape[0]
        w = 800
        h = int(w / aspect_ratio)

        img2 = cv2.resize(img, (w, h))

        try:
            if obj.prediction_results:
                prediction_results = json.loads(obj.prediction_results)
                for bbox in prediction_results.get("bboxes", []):
                    img2 = cv2.rectangle(
                        img2,
                        (int(bbox[1] * img2.shape[1]), int(bbox[0] * img2.shape[0])),
                        (int(bbox[3] * img2.shape[1]), int(bbox[2] * img2.shape[0])),
                        (0, 255, 0),
                        1,
                    )
        except Exception as ex:
            print(ex)

        base64Encoded = base64.b64encode(cv2.imencode(".jpg", img2)[1]).decode("utf8")
        return format_html('<img src="data:;base64,{}">', base64Encoded)


@admin.register(ResourceBatch)
class ResourceBatchAdmin(admin.ModelAdmin):
    list_display = [
        "id",
        "job_id",
        "processing_status",
        "processing_stage",
        "downloading_time",
        "prediction_time",
    ]
    actions = ["reprocess_batch"]

    list_filter = [
        "processing_status",
        "processing_stage",
    ]

    readonly_fields = [
        "processing_error_msg",
        "download_start_timestamp",
        "download_end_timestamp",
        "prediction_start_timestamp",
        "prediction_end_timestamp",
    ]

    def reprocess_batch(self, request, queryset):
        for batch in queryset:
            download_images.delay(batch.id)
