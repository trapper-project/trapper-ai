from math import ceil, floor
from typing import Union
import json

from rest_framework import serializers
from django.db.models import Q, Count
import structlog

from apps.api.tasks import download_images

from .models import PredictionJob, Resource, ResourceBatch
from apps.ai_models.models import PredictionModel


from django.utils.translation import gettext_lazy as _

from django.utils import timezone

from drf_spectacular.utils import (
    extend_schema_serializer,
    OpenApiExample,
    extend_schema_field,
)
from drf_spectacular.types import OpenApiTypes

logger = structlog.get_logger(__name__)


# extend_schema_field(
#     # ???
# )
class StatusField(serializers.ChoiceField):
    def to_representation(self, obj: Union[str, int]) -> dict:
        if obj == "" and self.allow_blank:
            return obj

        return {
            "value": obj,
            "label": self._choices[obj],
        }

    def to_internal_value(self, data: Union[str, int]):
        # To support inserts with the value
        if data == "" and self.allow_blank:
            return ""

        # if inserted by key
        if data in self.choices:
            return data

        # if inserted by name
        for key, val in self._choices.items():
            if val == data:
                return key

        self.fail("invalid_choice", input=data)


class TrapperSampleSerializer(serializers.Serializer):
    """
    Sample to process represented by the ID and resource URL
    """

    sample_id = serializers.IntegerField(
        help_text=_(
            "Sample identifier in your system. You can use this to identify resource processing results."
        )
    )
    sample_url = serializers.URLField(
        help_text=_("URL of the sample, accessible from the TrapperAI instance")
    )


class BatchWithStatusSerializer(serializers.ModelSerializer):
    processing_status = StatusField(choices=ResourceBatch.ProcesingStatus.choices)
    processing_stage = StatusField(choices=ResourceBatch.ProcessingStage.choices)

    class Meta:
        model = ResourceBatch
        fields = ["id", "processing_status", "processing_stage"]


class PredictionJobSerializer(serializers.ModelSerializer):
    """
    Trapper request data serializer
    """

    status = StatusField(choices=PredictionJob.PredictionJobStatus.choices)
    stage = StatusField(choices=PredictionJob.PredictionJobStage.choices)
    progress_metrics = serializers.SerializerMethodField()
    batches = BatchWithStatusSerializer(many=True)

    def get_progress_metrics(self, instance: PredictionJob) -> dict:
        """
        Return metrics about related tasks statuses
        """

        return instance.batches.aggregate(
            in_progress=Count(
                "pk",
                filter=~Q(
                    processing_stage__in=[ResourceBatch.ProcessingStage.FINISHED]
                ),
            ),
            finished=Count(
                "pk",
                filter=Q(
                    processing_stage=ResourceBatch.ProcessingStage.FINISHED,
                    processing_status=ResourceBatch.ProcesingStatus.SUCCEEDED,
                ),
            ),
            failed=Count(
                "pk",
                filter=Q(
                    # processing_stage=ResourceBatch.ProcessingStage.FINISHED,
                    processing_status=ResourceBatch.ProcesingStatus.FAILED,
                ),
            ),
        )

    class Meta:
        model = PredictionJob

        fields = [
            "id",
            "status",
            "stage",
            "progress_metrics",
            "batches",
        ]


@extend_schema_serializer(
    examples=[
        OpenApiExample(
            _("Simple request"),
            value={
                "ai_model_id": "{paste_model_uuid_here}",
                "samples": [
                    {
                        "sample_id": "123",
                        "sample_url": "https://upload.wikimedia.org/wikipedia/commons/8/8b/Moose_superior.jpg",
                    }
                ],
            },
            request_only=True,
        ),
        OpenApiExample(
            _("Batch size setting"),
            value={
                "ai_model_id": "{paste_model_uuid_here}",
                "samples": [
                    {
                        "sample_id": "123",
                        "sample_url": "https://upload.wikimedia.org/wikipedia/commons/8/8b/Moose_superior.jpg",
                    }
                ],
                "batch_size": 10,
            },
            request_only=True,
        ),
    ]
)
class CreatePredictionJobSerializer(serializers.ModelSerializer):
    """
    Create and run new Prediction Job
    """

    default_error_messages = {
        "inactive_model": _("Chosen AI model is not active"),
    }

    ai_model_id = serializers.PrimaryKeyRelatedField(
        queryset=PredictionModel.objects.all(),
        write_only=True,
        help_text=_("Identifier of the AI prediction model. Model must be active."),
    )
    samples = TrapperSampleSerializer(
        many=True, write_only=True, help_text=_("List of samples to process")
    )

    batch_size = serializers.IntegerField(
        help_text=_(
            "Number of smaples that will be processed by the TrapperAI at the same time. Cannot be larger than batch size allowed by AI Model"
        ),
        write_only=True,
        required=False,
    )

    processing_error_limit = serializers.IntegerField(
        write_only=True,
        required=False,
        help_text=_(
            "Maximum number of resource processing errors after which job will be canceled"
        ),
    )

    id = serializers.UUIDField(read_only=True)

    def create(self, validated_data):
        samples = validated_data.pop("samples")

        job = PredictionJob.objects.create(**validated_data)
        job.update_status(PredictionJob.PredictionJobStatus.IN_PROGRESS)

        no_batches = ceil(len(samples) / job.batch_size)
        batches = ResourceBatch.objects.bulk_create(
            [ResourceBatch(job=job) for _ in range(no_batches)]
        )

        resources = []
        for i, sample in enumerate(samples):
            resources.append(
                Resource(
                    job=job,
                    sample_id=sample.get("sample_id"),
                    sample_url=sample.get("sample_url"),
                    resource_batch=batches[floor(i / job.ai_model_id.batch_size)],
                )
            )

        Resource.objects.bulk_create(resources)

        for batch in batches:
            download_images.delay(batch.id)

        return job

    def validate(self, attrs):
        ai_model = attrs.get("ai_model_id")
        if ai_model and not ai_model.is_active:
            self.fail("inactive_model")

        batch_size = attrs.get("batch_size")
        if not batch_size or batch_size > ai_model.batch_size:
            attrs["batch_size"] = ai_model.batch_size

        processing_error_limit = attrs.get("processing_error_limit")
        if processing_error_limit is None:
            attrs["processing_error_limit"] = ai_model.processing_error_limit

        return super().validate(attrs)

    class Meta:
        model = PredictionJob

        fields = [
            "id",
            "ai_model_id",
            "samples",
            "batch_size",
            "processing_error_limit",
        ]


class ResourceSerializer(serializers.ModelSerializer):
    """
    Resource with prediction results
    """

    prediction_results = serializers.SerializerMethodField(read_only=True)

    status = StatusField(choices=Resource.ResourceProcessingStatus.choices)
    stage = StatusField(choices=Resource.ResourceProcessingStage.choices)

    def get_prediction_results(self, instance: Resource) -> Union[dict, None]:
        """
        Return prediction results (bboxes, predicted class and accuracy)
        """

        if instance.prediction_results:
            return json.loads(instance.prediction_results)

    class Meta:
        model = Resource

        exclude = ["origin_image", "sample_url"]
        read_only_fields = ["sample_id", "job", "resource_batch"]


class ResourceSerializerForPrediction(serializers.ModelSerializer):
    """
    Provide only basic information about the resource for prediction purposes.
    """

    status = StatusField(choices=Resource.ResourceProcessingStatus.choices)
    stage = StatusField(choices=Resource.ResourceProcessingStage.choices)

    class Meta:
        model = Resource
        fields = ["id", "status", "stage"]


class PredictionModelSerializer(serializers.ModelSerializer):
    class Meta:
        model = PredictionModel
        fields = ["id", "model_file_hash", "predictor_class", "model_config"]


class MinimalPredictionJobSerializer(serializers.ModelSerializer):
    prediction_model_id = serializers.UUIDField(source="ai_model_id.id")

    class Meta:
        model = PredictionJob
        fields = ["id", "prediction_model_id"]


class ResourceBatchSerializer(serializers.ModelSerializer):
    resources = ResourceSerializerForPrediction(many=True, read_only=True)

    processing_status = StatusField(choices=ResourceBatch.ProcesingStatus.choices)
    processing_stage = StatusField(choices=ResourceBatch.ProcessingStage.choices)
    processing_error_msg = serializers.CharField()
    job = MinimalPredictionJobSerializer(read_only=True)

    def update(self, instance: ResourceBatch, validated_data):
        new_stage_code = validated_data.get("processing_stage")
        new_status_code = validated_data.get("processing_status")
        logger.info(
            "ResourceBatchSerializer::update",
            new_stage_code=new_stage_code,
            new_status_code=new_status_code,
        )
        if new_stage_code is not None:
            if (
                new_stage_code
                == ResourceBatch.ProcessingStage.RESOURCE_DOWNLOADING_FINISHED
            ):
                # downloading finished
                instance.download_end_timestamp = timezone.now()
            elif new_stage_code == ResourceBatch.ProcessingStage.FINISHED:
                # prediction finished
                instance.prediction_end_timestamp = timezone.now()
            elif new_stage_code == ResourceBatch.ProcessingStage.RESOURCE_DOWNLOADING:
                # downloading started
                instance.download_start_timestamp = timezone.now()
                instance.download_end_timestamp = None
            elif new_stage_code == ResourceBatch.ProcessingStage.PREDICTION:
                # prediction started
                instance.prediction_start_timestamp = timezone.now()
                instance.prediction_end_timestamp = None

        if new_status_code is not None:
            if new_status_code == ResourceBatch.ProcesingStatus.FAILED:
                logger.warn(
                    "Batch failed",
                    all_resources=instance.resources.all().count(),
                    already_failed_resources=instance.resources.all()
                    .filter(
                        status=Resource.ResourceProcessingStatus.FAILED,
                    )
                    .count(),
                    currently_non_failed_resources=instance.resources.all()
                    .exclude(
                        status=Resource.ResourceProcessingStatus.FAILED,
                    )
                    .count(),
                )
                instance.resources.all().exclude(
                    status=Resource.ResourceProcessingStatus.FAILED,
                ).update(
                    status=Resource.ResourceProcessingStatus.FAILED,
                    processing_error_msg=_("Batch failed"),
                )

        return super().update(instance, validated_data)

    class Meta:
        model = ResourceBatch
        fields = [
            "id",
            "resources",
            "processing_stage",
            "processing_status",
            "processing_error_msg",
            "job",
        ]


class DetectedObjectSerializer(serializers.Serializer):
    bboxes = serializers.ListField(
        child=serializers.ListField(child=serializers.FloatField(), allow_null=True),
        help_text=_("Bounding boxes"),
    )
    confidence_per_frame = serializers.ListField(
        child=serializers.FloatField(
            required=False,
            allow_null=True,
        ),
        help_text=_("Confidence per frame"),
    )
    # confidence = serializers.FloatField(help_text=_("Confidence"))
    class_id = serializers.FloatField(help_text=_("Class ID"))

    def create(self, validated_data):
        return validated_data


class PredictionsSerializer(serializers.Serializer):
    bboxes = serializers.ListField(
        child=serializers.ListField(child=serializers.FloatField()),
        help_text=_("Bounding boxes"),
    )
    scores = serializers.ListField(
        child=serializers.FloatField(),
        help_text=_("List of scores for corresponding detections"),
    )
    classes = serializers.ListField(
        child=serializers.FloatField(),
        help_text=_("Detected classes for each bounding box"),
    )
    detected_objects = DetectedObjectSerializer(
        many=True,
        allow_null=True,
        required=False,
        help_text=_("List of detected objects"),
    )

    def create(self, validated_data):
        return validated_data


class ResourceProcessingResultSerializer(serializers.Serializer):
    id = serializers.UUIDField(help_text=_("Resource identifier"))
    errors = serializers.ListField(
        child=serializers.CharField(),
        allow_null=True,
        required=False,
        help_text=_("List of resource processing errors, if any"),
    )
    predictions = PredictionsSerializer(
        allow_null=True, required=False, help_text=_("Predictions for this resource")
    )

    def create(self, validated_data):
        return validated_data

    def validate(self, attrs):
        if not attrs.get("errors") and not attrs.get("predictions"):
            raise serializers.ValidationError(
                "either 'errors' or 'predictions' field is required"
            )
        return attrs


class RetrieveBatchResultsSerializer(serializers.ModelSerializer):

    resources = ResourceSerializer(many=True)

    class Meta:
        model = ResourceBatch
        fields = ["id", "resources"]
