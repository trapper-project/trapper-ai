import datetime
import json
from unittest import mock
from django.test import RequestFactory, TestCase

from apps.ai_models.models import AIRuntime, PredictionModel
from django.core.files.base import ContentFile

from rest_framework.test import APITestCase

from django.contrib.auth.models import User

from apps.api.models import PredictionJob, Resource, ResourceBatch
from rest_framework import status

from apps.api.tasks import BatchPredictionScheduler, refresh_prediction_job_status


class TestPredictionJobViews(APITestCase):
    def setUp(self) -> None:
        self.prediction_model = PredictionModel.objects.create(is_active=True)
        self.prediction_model.model_file.save(
            "test_model_file", ContentFile("test_model_file_contents")
        )
        self.prediction_model.save()

        self.request_factory = RequestFactory()
        self.client.force_authenticate(User.objects.create())

        download_images_task_patcher = mock.patch(
            "apps.api.tasks.download_images.apply_async"
        )
        self.download_images_task_mock = download_images_task_patcher.start()
        self.addCleanup(download_images_task_patcher.stop)

    def test_create_prediction_job(self):
        payload = {
            "ai_model_id": str(self.prediction_model.id),
            "samples": [
                {"sample_id": 0, "sample_url": "https://example.com/example-image.jpg"}
            ],
        }
        r = self.client.post("/api/prediction_jobs/", payload, format="json")

        self.assertEqual(r.status_code, 201)

        self.assertEqual(PredictionJob.objects.count(), 1)
        self.assertEqual(PredictionJob.objects.get().batches.count(), 1)
        self.download_images_task_mock.assert_called_once_with(
            (PredictionJob.objects.get().batches.get().pk,), {}
        )

    def test_batching(self):
        payload = {
            "ai_model_id": str(self.prediction_model.id),
            "samples": [
                {
                    "sample_id": sample_id,
                    "sample_url": "https://example.com/example-image.jpg",
                }
                for sample_id in range(self.prediction_model.batch_size * 5)
            ],
        }

        r = self.client.post("/api/prediction_jobs/", payload, format="json")

        self.assertEqual(PredictionJob.objects.get().batches.count(), 5)
        self.assertEqual(Resource.objects.count(), len(payload["samples"]))


class TestResourceBatchViews(APITestCase):
    def setUp(self) -> None:
        self.client.force_authenticate(User.objects.create())
        self.batch = ResourceBatch.objects.create(
            job=PredictionJob.objects.create(
                ai_model_id=PredictionModel.objects.create()
            )
        )

    @mock.patch("apps.api.tasks.refresh_prediction_job_status.apply_async")
    def test_save_results(self, refresh_prediction_job_status_mock):
        resources = [
            Resource.objects.create(sample_id=0, resource_batch=self.batch)
            for _ in range(10)
        ]
        example = {
            "bboxes": [
                [0, 0, 0, 0],
                [0, 0, 0, 0],
                [0, 0, 0, 0],
                [0, 0, 0, 0],
            ],
            "scores": [0.99, 0.55, 0.88, 0.78],
            "classes": [1, 2, 3, 4],
        }

        payload = [
            {"id": resource.id, "predictions": example} for resource in resources
        ]

        r = self.client.post(
            f"/api/resource_batches/{self.batch.id}/save_results/",
            payload,
            format="json",
        )

        self.assertEqual(r.status_code, status.HTTP_202_ACCEPTED)

        for res in resources:
            res.refresh_from_db()
            self.assertDictEqual(json.loads(res.prediction_results), example)

        refresh_prediction_job_status_mock.assert_called_once()

    def test_reject_on_missing_fields(self):
        resources = [
            Resource.objects.create(sample_id=0, resource_batch=self.batch)
            for _ in range(10)
        ]
        payload = [
            {
                "id": resource.id,
            }
            for resource in resources
        ]

        r = self.client.post(
            f"/api/resource_batches/{self.batch.id}/save_results/",
            payload,
            format="json",
        )

        self.assertEqual(r.status_code, status.HTTP_400_BAD_REQUEST)

    def test_save_results_with_errors(self):
        resources = [
            Resource.objects.create(sample_id=0, resource_batch=self.batch)
            for _ in range(10)
        ]

        payload = [
            {"id": resource.id, "errors": ["error1", "error2"]}
            for resource in resources
        ]

        r = self.client.post(
            f"/api/resource_batches/{self.batch.id}/save_results/",
            payload,
            format="json",
        )

        self.assertEqual(r.status_code, status.HTTP_202_ACCEPTED)

        for res in resources:
            res.refresh_from_db()
            self.assertEqual(res.prediction_results, "")
            self.assertEqual(res.status, Resource.ResourceProcessingStatus.FAILED)


class TestResourceBatch(TestCase):
    def test_downloading_time(self):
        cases = [
            (
                datetime.datetime(2023, 1, 17, 12, 35, 59, 805450),
                datetime.datetime(2023, 1, 17, 12, 36, 18, 742105),
                datetime.timedelta(seconds=18, microseconds=936655),
            ),
            (
                None,
                datetime.datetime(2023, 1, 17, 12, 36, 18, 742105),
                None,
            ),
            (
                datetime.datetime(2023, 1, 17, 12, 35, 59, 805450),
                None,
                None,
            ),
            (
                None,
                None,
                None,
            ),
        ]

        for start_time, end_time, dt in cases:
            batch = ResourceBatch.objects.create(
                download_start_timestamp=start_time, download_end_timestamp=end_time
            )
            self.assertEqual(batch.downloading_time, dt)

    def test_prediction_time(self):
        cases = [
            (
                datetime.datetime(2023, 1, 17, 12, 35, 59, 805450),
                datetime.datetime(2023, 1, 17, 12, 36, 18, 742105),
                datetime.timedelta(seconds=18, microseconds=936655),
            ),
            (
                None,
                datetime.datetime(2023, 1, 17, 12, 36, 18, 742105),
                None,
            ),
            (
                datetime.datetime(2023, 1, 17, 12, 35, 59, 805450),
                None,
                None,
            ),
            (
                None,
                None,
                None,
            ),
        ]

        for start_time, end_time, dt in cases:
            batch = ResourceBatch.objects.create(
                prediction_start_timestamp=start_time, prediction_end_timestamp=end_time
            )
            self.assertEqual(batch.prediction_time, dt)


class TestBatchPredictionScheduler(TestCase):
    def setUp(self) -> None:
        self.batch = ResourceBatch.objects.create()

    def test_schedule_batch_prediction(self):
        self.batch.runtime = AIRuntime.objects.create(name="test_runtime")
        self.batch.job = PredictionJob.objects.create(
            ai_model_id=PredictionModel.objects.create(
                runtime=AIRuntime.objects.create()
            )
        )
        self.batch.save()

        scheduler = BatchPredictionScheduler()

        with mock.patch("apps.api.tasks.app.send_task") as send_task_mock, mock.patch(
            "apps.ai_models.models.AIRuntime.get_runtime_queue_name",
            return_value="test_queue_name",
        ):
            scheduler.schedule_batch_prediction(self.batch.id)
            send_task_mock.assert_called_once_with(
                name="trapperai_runtime.run_batch_prediction",
                kwargs={"batch_id": self.batch.id},
                queue="test_queue_name",
            )

    def test_schedule_batch_prediction_no_job(self):
        self.batch.runtime = AIRuntime.objects.create(name="test_runtime")
        self.batch.save()

        scheduler = BatchPredictionScheduler()

        with mock.patch("apps.api.tasks.app.send_task") as send_task_mock, mock.patch(
            "apps.ai_models.models.AIRuntime.get_runtime_queue_name",
            return_value="test_queue_name",
        ):
            scheduler.schedule_batch_prediction(self.batch.id)
            send_task_mock.assert_not_called()
            self.batch.refresh_from_db()
            self.assertEqual(
                self.batch.processing_status, ResourceBatch.ProcesingStatus.FAILED
            )
            self.assertEqual(
                self.batch.processing_error_msg,
                scheduler.error_messages["job_not_defined"],
            )

    def test_schedule_batch_prediction_no_model(self):
        self.batch.runtime = AIRuntime.objects.create(name="test_runtime")
        self.batch.job = PredictionJob.objects.create()
        self.batch.save()

        scheduler = BatchPredictionScheduler()

        with mock.patch("apps.api.tasks.app.send_task") as send_task_mock, mock.patch(
            "apps.ai_models.models.AIRuntime.get_runtime_queue_name",
            return_value="test_queue_name",
        ):
            scheduler.schedule_batch_prediction(self.batch.id)
            send_task_mock.assert_not_called()
            self.batch.refresh_from_db()
            self.assertEqual(
                self.batch.processing_status, ResourceBatch.ProcesingStatus.FAILED
            )
            self.assertEqual(
                self.batch.processing_error_msg,
                scheduler.error_messages["model_not_defined"],
            )

    def test_schedule_batch_prediction_no_runtime(self):
        self.batch.runtime = AIRuntime.objects.create(name="test_runtime")
        self.batch.job = PredictionJob.objects.create(
            ai_model_id=PredictionModel.objects.create()
        )
        self.batch.save()

        scheduler = BatchPredictionScheduler()

        with mock.patch("apps.api.tasks.app.send_task") as send_task_mock, mock.patch(
            "apps.ai_models.models.AIRuntime.get_runtime_queue_name",
            return_value="test_queue_name",
        ):
            scheduler.schedule_batch_prediction(self.batch.id)

            send_task_mock.assert_not_called()
            self.batch.refresh_from_db()

            self.assertEqual(
                self.batch.processing_status, ResourceBatch.ProcesingStatus.FAILED
            )
            self.assertEqual(
                self.batch.processing_error_msg,
                scheduler.error_messages["runtime_not_defined"],
            )


class TestRefreshPredictionJobStatus(TestCase):
    def setUp(self) -> None:
        self.job = PredictionJob.objects.create()
        self.batches = [
            ResourceBatch.objects.create(
                job=self.job,
                processing_status=ResourceBatch.ProcesingStatus.IN_PROGRESS,
            )
            for _ in range(5)
        ]

    def test_refresh_prediction_job_all_in_progress(self):
        for batch in self.batches:
            for _ in range(10):
                Resource.objects.create(
                    resource_batch=batch,
                    status=Resource.ResourceProcessingStatus.IN_PROGRESS,
                    sample_id=123,
                )

        refresh_prediction_job_status(self.job.id)
        self.job.refresh_from_db()

        self.assertEqual(self.job.status, PredictionJob.PredictionJobStatus.IN_PROGRESS)

    def test_refresh_prediction_job_failed_less_than_threshold(self):
        self.job.processing_error_limit = 10
        self.job.save()
        failed_left = 5
        for batch in self.batches:
            for _ in range(10):
                if failed_left > 0:
                    Resource.objects.create(
                        resource_batch=batch,
                        status=Resource.ResourceProcessingStatus.FAILED,
                        sample_id=123,
                    )
                else:
                    Resource.objects.create(
                        resource_batch=batch,
                        status=Resource.ResourceProcessingStatus.IN_PROGRESS,
                        sample_id=123,
                    )
                failed_left -= 1

        refresh_prediction_job_status(self.job.id)

        self.job.refresh_from_db()
        self.assertEqual(self.job.status, PredictionJob.PredictionJobStatus.IN_PROGRESS)

    def test_refresh_prediction_job_failed_more_than_threshold(self):
        self.job.processing_error_limit = 10
        self.job.save()
        failed_left = 15
        for batch in self.batches:
            for _ in range(10):
                if failed_left > 0:
                    Resource.objects.create(
                        resource_batch=batch,
                        status=Resource.ResourceProcessingStatus.FAILED,
                        sample_id=123,
                    )
                else:
                    Resource.objects.create(
                        resource_batch=batch,
                        status=Resource.ResourceProcessingStatus.IN_PROGRESS,
                        sample_id=123,
                    )
                failed_left -= 1

        refresh_prediction_job_status(self.job.id)
        self.job.refresh_from_db()

        self.assertEqual(self.job.status, PredictionJob.PredictionJobStatus.FAILED)

    def test_all_batches_done(self):
        for batch in self.batches:
            batch.set_status(ResourceBatch.ProcesingStatus.SUCCEEDED)

        refresh_prediction_job_status(self.job.id)
        self.job.refresh_from_db()

        self.assertEqual(self.job.status, PredictionJob.PredictionJobStatus.DONE)
