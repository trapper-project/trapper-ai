import json
import math
import cv2
import numpy as np
import requests
import structlog
from django.db import transaction
from django.http import HttpResponse, StreamingHttpResponse
from django.utils.translation import gettext_lazy as _
from drf_spectacular.utils import extend_schema
from rest_framework import mixins, status, viewsets
from rest_framework.decorators import action
from rest_framework.response import Response

from apps.ai_models.models import PredictionModel

from .models import PredictionJob, Resource, ResourceBatch
from .serializers import (
    CreatePredictionJobSerializer,
    PredictionJobSerializer,
    PredictionModelSerializer,
    ResourceBatchSerializer,
    ResourceProcessingResultSerializer,
    ResourceSerializer,
)
from .tasks import refresh_prediction_job_status

logger = structlog.get_logger(__name__)


class ResourceBatchViewSet(
    mixins.RetrieveModelMixin, mixins.UpdateModelMixin, viewsets.GenericViewSet
):
    queryset = ResourceBatch.objects.all()
    serializer_class = ResourceBatchSerializer
    http_method_names = ["get", "patch", "post"]

    def partial_update(self, request, *args, **kwargs):
        r = super().partial_update(request, *args, **kwargs)
        obj: ResourceBatch = self.get_object()

        refresh_prediction_job_status.delay(obj.job_id)

        return r

    @extend_schema(
        request=ResourceProcessingResultSerializer(many=True),
        responses=ResourceBatchSerializer,
    )
    @action(
        detail=True,
        methods=["post"],
        serializer_class=ResourceProcessingResultSerializer,
    )
    def save_results(self, request, pk=None):
        batch: ResourceBatch = self.get_object()

        serializer = self.get_serializer(data=request.data, many=True)
        logger.info("save_results", data=request.data)
        if serializer.is_valid():
            data = serializer.save()

            results = {result["id"]: result for result in data}
            logger.info("results", results=results)

            with transaction.atomic():
                resources = batch.resources.all().select_for_update()
                resource: Resource
                for resource in resources:
                    results_for_resource = results[resource.id]

                    if results_for_resource.get("errors"):
                        resource.processing_error_msg = "; ".join(
                            results_for_resource["errors"]
                        )
                        resource.status = Resource.ResourceProcessingStatus.FAILED
                    elif results_for_resource.get("predictions"):
                        resource.prediction_results = json.dumps(
                            results_for_resource["predictions"]
                        )
                        resource.status = Resource.ResourceProcessingStatus.FINISHED
                        resource.stage = Resource.ResourceProcessingStage.FINISHED

                    if (
                        batch.job.ai_model_id.delete_images_after_processing
                        and resource.origin_image
                    ):
                        resource.origin_image.delete()

                Resource.objects.bulk_update(
                    resources, ["status", "processing_error_msg", "prediction_results"]
                )

            batch.set_stage(ResourceBatch.ProcessingStage.FINISHED)
            batch.processing_status = ResourceBatch.ProcesingStatus.SUCCEEDED
            batch.save(update_fields=["processing_status"])

            refresh_prediction_job_status.delay(batch.job_id)

            return Response(
                ResourceBatchSerializer(batch).data, status=status.HTTP_202_ACCEPTED
            )

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    @action(detail=True, serializer_class=ResourceSerializer)
    def results(self, request, pk=None):
        batch: ResourceBatch = self.get_object()
        # serializer = ResourceSerializer(batch.resources, many=True)

        return Response(
            data=ResourceSerializer(batch.resources.all(), many=True).data,
            status=status.HTTP_200_OK,
        )


class PredictionJobViewSet(
    mixins.RetrieveModelMixin, mixins.UpdateModelMixin, viewsets.GenericViewSet
):
    queryset = PredictionJob.objects.all()
    serializer_class = PredictionJobSerializer


class ResourceViewSet(
    mixins.RetrieveModelMixin, mixins.UpdateModelMixin, viewsets.GenericViewSet
):
    queryset = Resource.objects.all()
    serializer_class = ResourceSerializer
    http_method_names = ["get", "patch"]

    @extend_schema(responses=bytes)
    @action(detail=True, methods=["get"])
    def image(self, request, pk):
        """Retrieve raw image for given resource"""
        resource: Resource = self.get_object()
        if not resource.sample_url:
            return HttpResponse(
                content="Not found - no sample url specified for this object",
                status=404,
            )

        if resource.job.ai_model_id.enable_image_pass_through:
            try:
                r = requests.get(
                    resource.sample_url, timeout=25, headers={"User-Agent": "TrapperAI"}
                )
                if r.status_code != 200:
                    logger.error(
                        "Error downloading image from source server",
                        status_code=r.status_code,
                        url=resource.sample_url,
                        error=r.text,
                    )
                    return HttpResponse(
                        content="Error downloading image from source server", status=502
                    )
                return HttpResponse(
                    content=r.content,
                    content_type="image/*",
                    headers={
                        "Content-Disposition": "inline",
                    },
                )
            except requests.Timeout:
                return HttpResponse(content="Timeout exceeded", status=502)

        if not resource.origin_image:
            return HttpResponse(content="Not found", status=404)
        return StreamingHttpResponse(
            streaming_content=resource.origin_image.open(),
            content_type="image/*",
            headers={
                "Content-Disposition": "inline",
            },
        )

    @extend_schema(responses=bytes)
    @action(detail=True, methods=["get"])
    def anonymized_image(self, request, pk):
        """Retrieve raw image for given resource"""
        resource: Resource = self.get_object()

        ai_model: PredictionModel = resource.resource_batch.job.ai_model_id

        if not resource.origin_image and not ai_model.enable_image_pass_through:
            return HttpResponse(content="Not found", status=404)

        if not resource.sample_url:
            return HttpResponse(content="Not found", status=404)

        prediction_results = json.loads(resource.prediction_results)
        bboxes_to_anonymize = []
        bboxes = prediction_results["bboxes"]
        classes = prediction_results["classes"]
        scores = prediction_results["scores"]

        for score, bbox, pred_cls in zip(scores, bboxes, classes):
            if score < ai_model.anonimization_score_threshold:
                continue
            if int(pred_cls) in ai_model.anonimize_classes:
                bboxes_to_anonymize.append(bbox)

        if ai_model.enable_image_pass_through:
            with requests.get(resource.sample_url, stream=True) as r:
                img = np.asarray(bytearray(r.content), dtype="uint8")
                img = cv2.imdecode(img, cv2.IMREAD_COLOR)
        else:
            img = cv2.imread(resource.origin_image.path)

        img_h, img_w, _ = img.shape

        for yy1, xx1, yy2, xx2 in bboxes_to_anonymize:
            x1 = round(xx1 * img_w)
            y1 = round(yy1 * img_h)

            x2 = round(xx2 * img_w)
            y2 = round(yy2 * img_h)

            x1 = max(0, min(x1, img_w))
            x2 = max(0, min(x2, img_w))

            y1 = max(0, min(y1, img_h))
            y2 = max(0, min(y2, img_h))

            img[y1:y2, x1:x2] = cv2.GaussianBlur(
                img[y1:y2, x1:x2], (2 * 99 + 1, 2 * 99 + 1), 0
            )

        r, buf = cv2.imencode(".jpg", img)

        response = HttpResponse(
            content=buf.tobytes(),
            content_type="image/*",
            headers={
                "Content-Disposition": "inline",
            },
        )

        return response


class PredictionModelViewSet(mixins.RetrieveModelMixin, viewsets.GenericViewSet):
    queryset = PredictionModel.objects.all()
    serializer_class = PredictionModelSerializer

    @action(detail=True, methods=["get"])
    def model_file(self, request, pk):
        """Retrieve model file"""
        prediction_model: PredictionModel = self.get_object()
        return StreamingHttpResponse(
            streaming_content=prediction_model.model_file.open(),
            content_type="application/octet-stream",
            headers={
                "Content-Disposition": f"attachment; filename={prediction_model.model_file.name}",
            },
        )


class PredictionJobViewSet(
    mixins.RetrieveModelMixin, mixins.CreateModelMixin, viewsets.GenericViewSet
):
    queryset = PredictionJob.objects.all()

    def get_serializer_class(self):
        if self.action == "retrieve":
            return PredictionJobSerializer
        elif self.action == "create":
            return CreatePredictionJobSerializer
