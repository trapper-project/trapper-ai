import os
import uuid

import requests

from django.conf import settings
from tenacity import retry, stop_after_attempt, wait_fixed


class ImageDownloader:
    def __init__(self, sample_url) -> None:
        self.image_url = sample_url

    @retry(stop=stop_after_attempt(3), wait=wait_fixed(1))
    def get_image_contents(self) -> bytes:
        response = requests.get(
            self.image_url, verify=settings.REQUESTS_VERIFY_SSL, timeout=25
        )
        fname = os.path.basename(self.image_url.split("?")[0])
        fname, ext = os.path.splitext(fname)
        fname += "_" + str(uuid.uuid4())
        if ext:
            fname += f".{ext}"
        response.raise_for_status()

        return response.content, fname
