from rest_framework.routers import DefaultRouter

from .views import (
    PredictionJobViewSet,
    PredictionModelViewSet,
    ResourceViewSet,
    ResourceBatchViewSet,
)

router = DefaultRouter()
router.register("resource_batches", ResourceBatchViewSet, basename="resource_batch")
router.register("resources", ResourceViewSet, basename="resource")
router.register(
    "prediction_models", PredictionModelViewSet, basename="prediction_model"
)
router.register("prediction_jobs", PredictionJobViewSet, basename="prediction_job")

urlpatterns = router.urls
