# Generated by Django 2.2.26 on 2022-07-20 09:51

from django.db import migrations, models
import uuid


class Migration(migrations.Migration):

    dependencies = [
        ("api", "0016_auto_20220201_1412"),
    ]

    operations = [
        migrations.AlterField(
            model_name="trapperrequestdata",
            name="task_id",
            field=models.UUIDField(
                db_index=True,
                default=uuid.UUID("e9205afe-7bd9-493d-9ca1-46c5c47f06e0"),
                verbose_name="Trapper Task UUID",
            ),
        ),
    ]
