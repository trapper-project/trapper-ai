# Generated by Django 2.2.26 on 2022-01-31 11:18

from django.db import migrations, models
import uuid


class Migration(migrations.Migration):

    dependencies = [
        ("api", "0012_auto_20220128_0952"),
    ]

    operations = [
        migrations.RemoveField(
            model_name="trappertaskresults",
            name="image_matrix",
        ),
        migrations.AddField(
            model_name="trappertaskresults",
            name="origin_image",
            field=models.FileField(
                null=True, upload_to="transformed_images", verbose_name="NumPy matrix"
            ),
        ),
        migrations.AlterField(
            model_name="trapperrequestdata",
            name="task_id",
            field=models.UUIDField(
                db_index=True,
                default=uuid.UUID("b7a88686-0e34-48c9-a78a-d72bccb5f00a"),
                verbose_name="Trapper Task UUID",
            ),
        ),
    ]
