# Generated by Django 2.2.26 on 2022-02-01 13:43

from django.db import migrations, models
import uuid


class Migration(migrations.Migration):

    dependencies = [
        ("api", "0014_auto_20220131_1127"),
    ]

    operations = [
        migrations.AlterField(
            model_name="trapperrequestdata",
            name="task_id",
            field=models.UUIDField(
                db_index=True,
                default=uuid.UUID("0b870878-d71c-4ac9-a171-43198d7cf520"),
                verbose_name="Trapper Task UUID",
            ),
        ),
    ]
