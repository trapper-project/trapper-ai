# Generated by Django 3.2.16 on 2022-12-01 12:05

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ("ai_models", "0004_auto_20220201_1412"),
    ]

    operations = [
        migrations.AddField(
            model_name="predictionmodel",
            name="model_config",
            field=models.JSONField(default=dict),
        ),
    ]
