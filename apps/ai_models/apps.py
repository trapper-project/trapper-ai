from django.apps import AppConfig


class AIModelsConfig(AppConfig):
    name = "apps.ai_models"
    verbose_name = "AI models"
