from django.test import TestCase

from apps.ai_models.models import AIRuntime, PredictionModel
from django.core.files.base import ContentFile


class TestAIModel(TestCase):
    def test_hash_computation(self):
        model = PredictionModel()
        model.model_file.save("test_file", ContentFile("test_model_file_contents"))

        model.save()

        self.assertEqual(
            model.model_file_hash,
            "81854f2b8828a8f62ee5c1c73b059b52c701964b4f8439929a50505873000b71",
        )

    def test_hash_computation_on_empty_file(self):
        model = PredictionModel()
        model.save()

        model.save()

        self.assertEqual(
            model.model_file_hash,
            None,
        )


class TestAIRuntime(TestCase):
    def test_get_queue_name(self):
        cases = [
            ("test", "prefix__test"),
            ("Test name", "prefix__test_name"),
            ("test123", "prefix__test123"),
        ]
        with self.settings(AI_RUNTIME_QUEUE_PREFIX="prefix__"):

            for name, queue_name in cases:
                ai_runtime = AIRuntime(name=name)
                ai_runtime.save()

                self.assertEqual(ai_runtime.get_runtime_queue_name(), queue_name)
