from django.contrib import admin

from .models import AIRuntime, PredictionModel


@admin.register(AIRuntime)
class AIRuntimeModelAdmin(admin.ModelAdmin):
    list_display = ["name"]


@admin.register(PredictionModel)
class PredictionModelAdmin(admin.ModelAdmin):
    list_display = [
        "id",
        "name",
        "version",
        "is_active",
        "created_date",
    ]

    list_filter = ["is_active"]

    search_fields = ["id", "name", "version", "description"]

    readonly_fields = ["id", "created_date", "updated_date", "model_file_hash"]

    fieldsets = (
        ("About", {"fields": ("id", "name", "version", "description")}),
        (
            "Model data",
            {
                "fields": (
                    "model_file",
                    "model_file_hash",
                    "model_config",
                    "runtime",
                    "predictor_class",
                )
            },
        ),
        (
            "Config",
            {
                "fields": (
                    "is_active",
                    "delete_images_after_processing",
                    "enable_image_pass_through",
                    "batch_size",
                    "anonimize_classes",
                    "anonimization_score_threshold",
                    "processing_error_limit",
                )
            },
        ),
        ("Dates", {"fields": ("created_date", "updated_date")}),
    )
