#!/bin/bash

set -e

export IMAGE_NAME=registry.gitlab.com/oscf/trapper-ai

COLOR_RED_BRIGHT='\033[1;31m'
COLOR_GREEN_BRIGHT='\033[1;32m'
COLOR_BLUE_BRIGHT='\033[1;34m'
NO_COLOR='\033[0m' # No Color

# Defaults
export ALLOWED_HOSTS="*"
export FLOWER_USERNAME=trapperai
export APP_PORT=8090
export RABBIT_PORT=8072
export TRAPPERAI_LOG_LEVEL=info
export DEV_MODE=0
export STAGING_SWITCH=""
export REQUESTS_VERIFY_SSL=1
export WEB_CONCURRENCY=8

export COMPOSE_PROJECT_NAME="trapper-ai"


CODE_DIR=$(pwd)

ACTIVE_BRANCH=master

# Global variables
fragments=()

# Utility functions

load_dotenv() {
  if [ -f .env ]; then
    print_info "Loading environment variables from .env file..."
    . .env
  else
    print_info "No .env file, skipping..."
  fi
}

print_err() {
  echo -e "${COLOR_RED_BRIGHT}[ERROR]${NO_COLOR} $1"
  exit 1
}
print_ok() {
  echo -e "${COLOR_GREEN_BRIGHT}[OK]${NO_COLOR} $1"
}
print_info() {
  echo -e "${COLOR_BLUE_BRIGHT}[INFO]${NO_COLOR} $1"
}

print_warn() {
  echo -e "${COLOR_RED_BRIGHT}[WARN]${NO_COLOR} $1"
}

# Deployment functions

build_backend() {
  print_info "Building backend and worker image"
  docker build . -t "${IMAGE_NAME}:${IMAGE_TAG_WEB}"
}

build_images() {
  build_backend
}

pull_images() {
  docker pull "${IMAGE_NAME}:${IMAGE_TAG_WEB}"
  docker pull "${IMAGE_NAME}:${IMAGE_TAG_WORKER}"
  docker compose "${compose_files[@]}" pull
}

export_image_tags() {
  if [ "$BUILD_IMAGES" = 1 ]; then
    ACTIVE_BRANCH=local
  fi
  export IMAGE_TAG_WEB=$ACTIVE_BRANCH
  export IMAGE_TAG_WORKER=$IMAGE_TAG_WEB
}

obtain_images() {
  if [ "$BUILD_IMAGES" = 1 ]; then
    print_info "BUILD_IMAGES flag enabled, building images"
    build_images
  else
    print_info "Images will be pulled from Docker registry"
    pull_images
  fi
}

configure_volumes() {
  if [ "${MOUNT_VOLUMES}" = 1 ]; then
    if [ -z "$VOLUMES_DIR" ]; then
      VOLUMES_DIR="$(pwd)/volumes"
      print_warn "VOLUMES_DIR not set, defaulting to ${VOLUMES_DIR}"
    fi

    print_ok "Mounting volumes to ${VOLUMES_DIR}"
    export VOLUMES_DIR
    fragments+=("volumes")
  else
    print_warn "Using named volumes for storing data"
  fi
}

configure_dev_mode() {
  if [ "$DEV_MODE" = 1 ]; then
    fragments+=("dev")
    print_info "Enabling development mode"
    export DEV_MODE CODE_DIR
  fi
}

check_required_env_var() {
  local var=$1
  if [ -n "${!var}" ]; then
    print_ok "$var set"
  else
    print_err "Required environment variable $var not present, aborting"
  fi
}

configure_ssl() {
  if [ "$ENABLE_SSL" = 1 ]; then
    export TRAPPERAI_DOMAIN
    check_required_env_var TRAPPERAI_DOMAIN

    if [ "$AUTO_SSL" = 1 ]; then
      print_info "Using auto ssl"
      fragments+=("ssl-auto")
    else
      print_info "Using static SSL certificates"
      local cert_path="./certs"
      local privkey="${cert_path}/privkey.pem"
      local fullchain="${cert_path}/fullchain.pem"

      if [ ! -f "$privkey" ]; then
        print_err "$privkey not present, aborting"
      elif [ ! -f "$fullchain" ]; then
        print_err "$fullchain not present, aborting"
      else
        print_ok "SSL keys present"
      fi
      fragments+=("ssl")
    fi
  else
    print_info "Not using SSL"
    fragments+=("no-ssl")
  fi
}



check_required_env_variables() {
  required_vars=("DB_USER" "DB_PASS" "SECRET_KEY" "FLOWER_PASSWORD")
  print_info "Checking basic configuration"
  for var in "${required_vars[@]}"; do
    check_required_env_var "$var"
    export "${var?}"
  done
}

compose_files=()
aggregate_compose_files() {
  for fragment in "${fragments[@]}"; do
    compose_files+=("-f" "docker/docker-compose.${fragment}.yml")
  done
}

prepare_config() {
  load_dotenv
  check_required_env_variables

  fragments+=("base")

  configure_volumes
  configure_dev_mode
  configure_ssl

  aggregate_compose_files
}

start() {
  print_info "Compose files: ${compose_files[*]}"
  docker compose "${compose_files[@]}" up --remove-orphans
}

start_detached() {
  print_info "Compose files: ${compose_files[*]}"
  docker compose "${compose_files[@]}" up -d --remove-orphans
}

stop() {
  docker compose "${compose_files[@]}" down --remove-orphans
}

logs() {
  docker compose "${compose_files[@]}" logs -f
}

docker_shell() {
  print_info "Entering TrapperAI container"
  docker compose "${compose_files[@]}" run --rm web bash
}

main() {
  local cmd=$1
  prepare_config
  export_image_tags

  if [ "$cmd" = "start" ]; then
    obtain_images
    start_detached
  elif [ "$cmd" = "start-i" ]; then
    obtain_images
    start
  elif [ "$cmd" = "stop" ]; then
    stop
  elif [ "$cmd" = "logs" ]; then
    logs
  elif [ "$cmd" = "shell" ]; then
    docker_shell
  else
    print_err "Invalid command: ${cmd}"
  fi
}

main "$1"
