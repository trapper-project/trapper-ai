FROM python:3.11-slim

RUN apt-get update && \
    apt-get install \
    build-essential \
    ffmpeg libsm6 libxext6 \
    libpq-dev \
    -y --no-install-recommends

ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1
RUN mkdir /code

ADD requirements.txt /code
WORKDIR /code

RUN pip install --upgrade pip --no-cache-dir
RUN pip install -r requirements.txt --no-cache-dir

ADD . /code

ENV TRAPPERAI_LOG_LEVEL=info

CMD ["./bin/web.sh"]
